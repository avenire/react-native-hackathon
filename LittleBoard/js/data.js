function getEvent (name, date) {
	return {name, date}
};

async function getEvents () {
	return Promise.resolve([
		getEvent("stage","21 marca"),
		getEvent("prod", "25 marca"),
		getEvent("stage","21 marca"),
		getEvent("prod", "25 marca")
	]);
};

export default getEvents;