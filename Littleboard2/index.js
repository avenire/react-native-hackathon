import { AppRegistry } from "react-native";
import App from "./App";
import AddEventScreen from "./js/AddEventScreen";
import { StackNavigator } from "react-navigation";

const stackNavigator = StackNavigator({
  Home: { screen: App },
  AddEventScreen: { screen: AddEventScreen }
});

AppRegistry.registerComponent("Littleboard2", () => stackNavigator);
