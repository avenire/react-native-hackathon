/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Button,
  DeviceEventEmitter
} from "react-native";

import getEvents from "./js/data";
import EventRow from "./js/EventRow";
const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

export default class App extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: "Welcome",
    headerRight: (
      <TouchableHighlight
        onPress={() => {
          navigation.navigate("AddEventScreen");
        }}
        style={{ padding: 10, marginRight: 10, backgroundColor: "green" }}
      >
        <Text style={{ color: "white" }}>Add</Text>
      </TouchableHighlight>
    )
  });

  constructor() {
    super();
    this.state = {
      events: []
    };
  }

  componentWillMount() {
    DeviceEventEmitter.addListener("listener", e => {
      this.fetchData();
    });
  }

  fetchData = async () => {
    console.log("Hello");
    const events = await getEvents();
    this.setState({ events });
  };

  componentDidMount() {
    this.fetchData();
  }

  onPress = event => {
    const events = this.state.events;
    events.splice(events.indexOf(event), 1);
    this.setState({
      events: events
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{ flex: 1, width: "100%" }}
          contentContainerStyle={{}}
          keyExtractor={event => event.id}
          data={this.state.events}
          renderItem={({ item }) => (
            <EventRow onPress={this.onPress} event={item} />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
