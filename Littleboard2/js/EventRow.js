import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableNativeFeedback,
  TouchableOpacity
} from "react-native";

export default class EventRow extends React.Component {
  static defaultProps = {
    event: {},
    onPress: E => {
      console.log("funkcja dziala");
    }
  };

  render() {
    const event = this.props.event;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{event.name}</Text>
        <Text style={styles.date}>{event.date}</Text>
        <TouchableOpacity
          style={{ width: 50, height: 50 }}
          onPress={() => this.props.onPress(this.props.event)}
        >
          <Text style={styles.button}>X</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
    marginBottom: 0,
    justifyContent: "center",
    alignItems: "flex-start",
    backgroundColor: "black",
    flexDirection: "row",
    borderColor: "white",
    borderBottomWidth: 2
  },
  text: {
    fontSize: 20,
    textAlign: "left",
    margin: 10,
    marginLeft: 10,
    color: "white",
    flex: 0.45
  },
  date: {
    fontSize: 20,
    textAlign: "right",
    color: "white",
    margin: 10,
    marginRight: 15,
    flex: 0.45
  },
  button: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    flex: 1,
    backgroundColor: "red",
    color: "white",
    borderRadius: 20
  }
});
