import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Button,
  DeviceEventEmitter,
  TextInput
} from "react-native";
import DatePicker from "react-native-datepicker";
import getEvents, { addEvent, createEvent } from "./data";
import EventRow from "./EventRow";
import moment from "moment";
const FORMAT = "YYYY-MM-DD";
export default class AddEventScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    console.log("navop1");
    console.log(navigation.state.params);
    const isValid = navigation.state.params
      ? navigation.state.params.isValid
      : false;
    console.log("navop2");
    console.log(isValid);
    return {
      title: screenProps.title || "Add event",
      headerRight: (
        <TouchableOpacity
          disabled={!isValid}
          onPress={async () => {
            const event = await navigation.state.params.onAdd();
            if (event) {
              DeviceEventEmitter.emit("listener", { event });
              navigation.goBack();
            }
          }}
          style={{
            padding: 10,
            marginRight: 10,
            backgroundColor: isValid ? "green" : "gray"
          }}
        >
          <Text style={{ color: "white" }}>Done</Text>
        </TouchableOpacity>
      )
    };
  };

  constructor() {
    super();
    const minDate = moment(new Date(Date.now())).format(FORMAT);
    this.state = {
      name: "",
      date: minDate,
      minDate
    };
  }

  validateEvent = event => {
    return (
      event &&
      event.name &&
      event.name.trim().length > 0 &&
      isNaN(Date.parse(event.date)) == false
    );
  };

  componentDidMount() {
    const navigation = this.props.navigation;
    navigation.setParams({
      onAdd: this.onAdd,
      isValid: false
    });
    this.checkIfValid(this.state.name, this.state.date);
  }

  onAdd = () => {
    const event = createEvent(this.state.name, this.state.date);
    if (!this.validateEvent(event)) {
      return Promise.resolve(null);
    }
    return addEvent(event);
  };

  checkIfValid = (name, date) => {
    const navigation = this.props.navigation;
    const e = { name, date };
    const isValid = !!this.validateEvent(e);
    console.log(e);
    console.log("is valid " + isValid);
    navigation.setParams({ isValid, onAdd: this.onAdd });
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          placeholder="Name of event"
          style={styles.textInput}
          onChangeText={name => {
            this.setState({ name });
            this.checkIfValid(name, this.state.date);
          }}
          value={this.state.name}
        />

        <DatePicker
          mode="date"
          style={{ width: 200 }}
          format={FORMAT}
          confirmBtnText="OK"
          date={this.state.date}
          cancelBtnText="Cancer"
          placeholder="select date"
          minDate={this.state.minDate}
          onDateChange={date => {
            this.setState({ date });
            this.checkIfValid(this.state.name, date);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  textInput: {
    fontSize: 25,
    textAlign: "center",
    margin: 10,
    width: 200
  }
});
