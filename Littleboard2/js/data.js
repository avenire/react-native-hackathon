var nextId = 100;
export function createEvent(name, date) {
  return { id: nextId++, name, date };
}

var events = [
  createEvent("piwo", "2017-03-21"),
  createEvent("mikolajki", "2017-03-25"),
  createEvent("wakacje", "2017-02-21"),
  createEvent("andrzejki", "2017-03-21"),
  createEvent("wesele", "2017-03-25"),
  createEvent("wyspianskiego", "2017-03-28")
];

export const addEvent = async event => {
  events = [...events, event];
  return Promise.resolve(event);
};

async function deleteEvent(event) {
  events.splice(events.indexOf(event), 1);
  return Promise.resolve();
}

async function getEvents() {
  return Promise.resolve(events);
}

export default getEvents;
